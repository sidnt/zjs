val scala3Version = "3.0.0-M3"

lazy val root = project
  .in(file("."))
  .enablePlugins(ScalaJSPlugin)
  .settings(
    name := "zjs",
    version := "0.1.0",
    scalaJSUseMainModuleInitializer := true,
    scalaVersion := scala3Version,
    libraryDependencies += ("dev.zio" %%% "zio" % "1.0.4").withDottyCompat(scalaVersion.value)
  )
